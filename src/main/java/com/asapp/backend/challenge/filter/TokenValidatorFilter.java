package com.asapp.backend.challenge.filter;

import static spark.Spark.halt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.asapp.backend.challenge.auth.objects.AuthenticationTool;
import com.asapp.backend.challenge.auth.services.AuthService;
import com.asapp.backend.challenge.user.model.User;
import com.asapp.backend.challenge.user.services.UserService;

import io.jsonwebtoken.Claims;
import spark.Filter;
import spark.Request;
import spark.Response;

@Component
public class TokenValidatorFilter {
	private static final String INVALID_TOKEN = "Invalid token";
	protected static AuthService authService;
	protected static UserService userService;
	protected static AuthenticationTool authTool;

	@Autowired
	private TokenValidatorFilter(AuthService authService, UserService userService, AuthenticationTool authTool) {
		TokenValidatorFilter.authService = authService;
		TokenValidatorFilter.userService = userService;
		TokenValidatorFilter.authTool = authTool;
	}

	public static final Filter validateUser = (Request req, Response resp) -> {
		String[] authStrings = null;
		String receivedAuthToken = null;
		String authType = null;
		Claims claims = null;
		User tokenUser = null;
		try {
			authStrings = req.headers("Authorization").split(" ");
			authType = authStrings[0];
			receivedAuthToken = authStrings[1];
		} catch (Exception e) {
			halt(401, INVALID_TOKEN);
		}
		if (!authType.contentEquals("Bearer")) {
			halt(401, INVALID_TOKEN);
		}

		try {
			claims = authTool.decodeJWT(receivedAuthToken);
			tokenUser = userService.findById((Integer) claims.get("userId"));
			
			if (!authService.findById(tokenUser.getId()).getToken().equals(receivedAuthToken)) {
					halt(401, INVALID_TOKEN);
				}
			
			
			
		} catch (Exception e) {
			halt(401, INVALID_TOKEN);
		}
		
	};
	
	
	

}

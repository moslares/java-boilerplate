package com.asapp.backend.challenge;

import static spark.Spark.before;
import static spark.Spark.get;
import static spark.Spark.post;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.asapp.backend.challenge.controller.AuthController;
import com.asapp.backend.challenge.controller.HealthController;
import com.asapp.backend.challenge.controller.MessagesController;
import com.asapp.backend.challenge.controller.UsersController;
import com.asapp.backend.challenge.filter.TokenValidatorFilter;
import com.asapp.backend.challenge.shared.utils.Path;

import spark.Spark;

@SpringBootApplication
public class Application {
	
	
	public static final int PORT = 8888;

	public static void main(String[] args) {
		
		// Spark Configuration
		Spark.port(PORT);

		// Configure Endpoints
		// Users
		
		post(Path.USERS, UsersController.createUser);
		// Auth
		post(Path.LOGIN, AuthController.login);
		// Messages
		before(Path.MESSAGES, TokenValidatorFilter.validateUser);
		post(Path.MESSAGES, MessagesController.sendMessage);
		get(Path.MESSAGES, MessagesController.getMessages);
		// Health
		post(Path.HEALTH, HealthController.check);
		
		SpringApplication.run(Application.class);

	}


}

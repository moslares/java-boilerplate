package com.asapp.backend.challenge.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = {"com.asapp.backend.challenge"})
public class ChallengeConfiguration {

   
}

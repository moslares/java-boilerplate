package com.asapp.backend.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.asapp.backend.challenge.resources.HealthResource;
import com.asapp.backend.challenge.shared.utils.JSONUtil;

import spark.Request;
import spark.Response;
import spark.Route;

@Component
public class HealthController {

	private static String testQuery;
	private static String jdbcUrl;

	@Autowired
	private HealthController(@Value(value = "${spring.datasource.test-query}") String testQuery, @Value(value = "${spring.datasource.url}") String jdbcUrl) {
		HealthController.testQuery = testQuery;
		HealthController.jdbcUrl = jdbcUrl;
	}

	public static final Route check = (Request req, Response rep) -> {
		rep.type(JSONUtil.APPLICATION_JSON);
		return JSONUtil.dataToJson(new HealthResource(jdbcUrl, testQuery));
	};

	
}

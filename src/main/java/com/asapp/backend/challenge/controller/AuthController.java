package com.asapp.backend.challenge.controller;

import static spark.Spark.halt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.asapp.backend.challenge.auth.objects.AuthenticationTool;
import com.asapp.backend.challenge.resources.LoginResource;
import com.asapp.backend.challenge.shared.responses.ResponseUtils;
import com.asapp.backend.challenge.shared.utils.JSONUtil;
import com.asapp.backend.challenge.user.model.User;
import com.asapp.backend.challenge.user.services.UserService;

import io.jsonwebtoken.Claims;
import spark.Request;
import spark.Response;
import spark.Route;

@Component
public class AuthController {

	protected static UserService userService;
	protected static AuthenticationTool authTool;

	@Autowired
	private AuthController(UserService userService, AuthenticationTool authTool) {
		AuthController.userService = userService;
		AuthController.authTool = authTool;

	}

	/**
	 * User login
	 */
	public static final Route login = (Request req, Response resp) -> {
		User user = (User) JSONUtil.jsonToData(req.body(), User.class);
		Claims claims = null;

		try {
			user.setAuthentication(userService.validateUserCredentials(user));
		} catch (Exception e) {
			halt(401, ResponseUtils.INVALID_USER);
		}
		if (user.getAuthentication() == null) {
			halt(401, ResponseUtils.INVALID_USER);
		} else {
			claims = authTool.decodeJWT(user.getAuthentication().getToken());
		}
		resp.type(JSONUtil.APPLICATION_JSON);

		return JSONUtil.dataToJson(new LoginResource(claims.get("userId"), user.getAuthentication().getToken()));
	};

}

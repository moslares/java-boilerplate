package com.asapp.backend.challenge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.asapp.backend.challenge.auth.objects.AuthenticationTool;
import com.asapp.backend.challenge.message.model.Message;
import com.asapp.backend.challenge.message.services.MessageService;
import com.asapp.backend.challenge.resources.MessageResource;
import com.asapp.backend.challenge.shared.responses.AddMessageResponse;
import com.asapp.backend.challenge.shared.responses.ResponseUtils;
import com.asapp.backend.challenge.shared.utils.JSONUtil;

import io.jsonwebtoken.Claims;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

@Component
public class MessagesController {

	protected static MessageService messagesService;
	protected static AuthenticationTool authTool;

	@Autowired
	private MessagesController(MessageService messagesService, AuthenticationTool authTool) {
		MessagesController.messagesService = messagesService;
		MessagesController.authTool = authTool;
	}

	/**
	 * Send a message
	 */
	public static final Route sendMessage = (Request req, Response rep) -> {
		Message message = (Message) JSONUtil.jsonToData(req.body(), Message.class);
		String token = null;
		try {
			token = req.headers("Authorization").split(" ")[1];
		} catch (Exception e) {
			Spark.halt(403, ResponseUtils.FORBIDDEN);
		}

		if (!verifyUserSendingIsSender(token, message.getSender().getId())) {
			Spark.halt(401, ResponseUtils.CANT_SEND_MESSAGE_FROM_ANOTHER_USER);

		}

		if (!usersExist(message.getSender().getId(), message.getRecipient().getId())) {
			Spark.halt(401, ResponseUtils.USER_NOT_EXISTANT);
		}

		Integer messageId = null;
		try {
			messageId = messagesService.addMessage(message);
		} catch (Exception e) {
			Spark.halt(401, ResponseUtils.INVALID_USER);
		}
		rep.type(JSONUtil.APPLICATION_JSON);

		return JSONUtil.dataToJson(new AddMessageResponse(messageId));
	};

	/**
	 * Get messages from receip within a limit
	 */
	public static final Route getMessages = (Request req, Response rep) -> {
		Integer recipient = Integer.valueOf(req.queryParams("recipient"));
		Integer start = Integer.valueOf(req.queryParams("start"));
		Integer limit = Integer.valueOf(req.queryParams("limit"));
		List<Message> messages = messagesService.getFilteredMessages(recipient, start, limit);

		rep.type(JSONUtil.APPLICATION_JSON);		
		return JSONUtil.dataToJson(new MessageResource(messages));
	};

	private static boolean usersExist(Integer senderId, Integer recipientId) {
		return (UsersController.userExist(senderId) && UsersController.userExist(recipientId));
	}

	private static final boolean verifyUserSendingIsSender(String token, Integer sender) {
		try {
			Claims claims = authTool.decodeJWT(token);
			Integer tokenUserId = (Integer) claims.get("userId");
			if (tokenUserId.equals(sender)) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

}

package com.asapp.backend.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.asapp.backend.challenge.resources.UserResource;
import com.asapp.backend.challenge.shared.responses.ResponseUtils;
import com.asapp.backend.challenge.shared.utils.JSONUtil;
import com.asapp.backend.challenge.user.model.User;
import com.asapp.backend.challenge.user.services.UserService;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

@Component
public class UsersController {

	protected static UserService userService;

	@Autowired
	private UsersController(UserService userService) {
		UsersController.userService = userService;
	}

	/**
	 * Route to create a new user
	 */
	public static final Route createUser = (Request req, Response resp) -> {

		User user = (User) JSONUtil.jsonToData(req.body(), User.class);

		if (null == user) {
			Spark.halt(401, ResponseUtils.INVALID_USER);
			}
		User userInDatabase = userService.findFirstByUsername(user.getUsername());

		if (userInDatabase != null) {
			Spark.halt(401, ResponseUtils.USER_ALLREADY_EXIST);
		}

		Integer userId = null;
		try {
			userId = userService.addUser(user);
		} catch (Exception e) {
			Spark.halt(500, ResponseUtils.GENERIC_ERROR);
		}

		resp.type(JSONUtil.APPLICATION_JSON);
		return JSONUtil.dataToJson(new UserResource(userId));
	};
	
	
	public static boolean userExist(Integer userId) {
		return userService.existsById(userId);
	}
}

package com.asapp.backend.challenge.resources;

import java.util.List;

import com.asapp.backend.challenge.message.model.Message;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageResource {
	private List<Message> messages;

	public MessageResource(List<Message> messages) {
		this.messages = messages;
	}

}

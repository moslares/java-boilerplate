package com.asapp.backend.challenge.resources;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.sqlite.SQLiteConfig;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HealthResource {

	private String health;

	public HealthResource(String jdbcUrl, String testQuery) {
		this.health = "down";

		try {

			int errorCode = check(jdbcUrl, testQuery);
			if (errorCode != 1)
				this.health = "down";
			this.health = "ok";
		} catch (Exception ex) {
			this.health = "down";
		}
	}

	private static int check(String jdbcUrl, String testQuery) throws SQLException {
		int result = 0;
		SQLiteConfig config = new SQLiteConfig();
		config.setReadOnly(true);

		try (Connection connection = config.createConnection(jdbcUrl);
				Statement statement = connection.createStatement()) {

			ResultSet resultSet = statement.executeQuery(testQuery);

			while (resultSet.next()) {
				result = Integer.parseInt(resultSet.getString(1));
			}

		} catch (Exception e) {
			return result;
		}
		return result;
	}
}

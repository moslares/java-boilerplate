package com.asapp.backend.challenge.resources;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResource {
	private Integer id;
	private String token;

	public LoginResource(Object id, String token) {
		this.id = (Integer) id;
		this.token = token;

	}
	public LoginResource(Integer id, String token) {
		this.id = id;
		this.token = token;

	}
	
	public LoginResource() {		
	}
}

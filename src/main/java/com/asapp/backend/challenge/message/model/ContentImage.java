package com.asapp.backend.challenge.message.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Setter;


@Setter
@Entity
@DiscriminatorValue("image")
@JsonDeserialize(using = JsonDeserializer.None.class)
public class ContentImage extends Content{
	
	/**
	 * Image content id
	 */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", updatable=false, nullable=false)
    protected Integer id;
	
	/**
	 * Image URL
	 */
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Column(name = "url")
	@JsonProperty("url")
	public String url;
	
	
	/**
	 * Image Height
	 */
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Column(name = "height")
	@JsonProperty("height")
	public Integer height;
	
	/**
	 * Image Width
	 */
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Column(name = "width")
	@JsonProperty("width")
	public Integer width;
	
	/**
	 * Image Content Constructor
	 */
	public ContentImage() {
		// default constructor
	}
		
	
	
}
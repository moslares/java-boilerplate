package com.asapp.backend.challenge.message.objects;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;

import com.asapp.backend.challenge.message.model.ContentImage;
import com.asapp.backend.challenge.message.model.ContentText;
import com.asapp.backend.challenge.message.model.ContentVideo;
import com.asapp.backend.challenge.message.model.Message;
import com.asapp.backend.challenge.user.model.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class MessageCustomDeserializer extends StdDeserializer<Message> {
	private static final long serialVersionUID = -7335686872154505105L;

	public MessageCustomDeserializer() {
		super(Message.class);
	}

	@Override
	public Message deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
		if (jp.getCurrentToken() != JsonToken.START_OBJECT) {
			throw new IOException("invalid start marker");
		}
		Message p = new Message();
		while (jp.nextToken() != JsonToken.END_OBJECT) {
			String fieldname = jp.getCurrentName();
			jp.nextToken(); // move to next token in string
			if ("id".equals(fieldname)) {
				p.setId(jp.getIntValue());
			} else if ("timestamp".equals(fieldname)) {
				p.setTimestamp(Date.valueOf(jp.getText()));
			} else if ("sender".equals(fieldname)) {
				p.setSender(new User(jp.getIntValue()));
			} else if ("recipient".equals(fieldname)) {
				p.setRecipient(new User(jp.getIntValue()));
			} else if ("content".equals(fieldname)) {
				ObjectCodec oc = jp.getCodec();
				JsonNode node = oc.readTree(jp);
				String type = node.get("type").textValue().toLowerCase();
				if (type.equals("text")) {
					p.setContent(jp.getCodec().treeToValue(node, ContentText.class));
				} else if (type.equals("image")) {
					p.setContent(jp.getCodec().treeToValue(node, ContentImage.class));
				} else if (type.equals("video")) {
					p.setContent(jp.getCodec().treeToValue(node, ContentVideo.class));
				}
			}
		}
		jp.close();
		verifyTimestamp(p);
		return p;
	}

	private void verifyTimestamp(Message p) {
		if (p != null) {
			try {
				p.getTimestamp();
			} catch (Exception e) {
				p.setTimestamp(Date.valueOf(LocalDate.now()));
			}
		}
	}

}
package com.asapp.backend.challenge.message.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "challengeapp_messages_content")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Content {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	@JsonIgnore
	private int id;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Column(name = "type", nullable = false)
	String type;
}

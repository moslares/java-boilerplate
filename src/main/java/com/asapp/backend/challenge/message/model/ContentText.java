package com.asapp.backend.challenge.message.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Setter;

@Setter
@Entity
@DiscriminatorValue("text")
@JsonDeserialize(using = JsonDeserializer.None.class)
public class ContentText extends Content{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", updatable=false, nullable=false)
    protected Integer id;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Column(name = "text")
	@JsonProperty("text")
	public String text;
	
	public ContentText() {
		// default constructor
	}
	

	
}
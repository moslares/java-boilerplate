package com.asapp.backend.challenge.message.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.asapp.backend.challenge.message.objects.Source;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Setter;

@Setter
@Entity
@DiscriminatorValue("video")
@JsonDeserialize(using = JsonDeserializer.None.class)
public class ContentVideo extends Content{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", updatable=false, nullable=false)
    protected Integer id;
	
	/**
	 * Video url
	 */
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Column(name = "url")
	@JsonProperty("url")
	public String url;
	
	/**
	 * Video source, see com.asapp.backend.challenge.message.objects.Source class
	 */
	@Enumerated(EnumType.STRING)
	@JsonIgnore
	private Source source;
	
	public ContentVideo() {
		// default constructor
	}
	
	@JsonProperty("timestamp")
	public String getSource() {
		return this.source.name().toLowerCase();
	}

	
}
package com.asapp.backend.challenge.message.model;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.asapp.backend.challenge.message.objects.MessageCustomDeserializer;
import com.asapp.backend.challenge.user.model.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author mmoslares
 * @see
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "challengeapp_messages")
@JsonDeserialize(using = MessageCustomDeserializer.class)
public class Message {

	/**
	 * Message id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	protected Integer id;

	/**
	 * Date and time stamp when the message is being received
	 */
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
	private Date timestamp;

	/**
	 * Message producer
	 */
	@ManyToOne
	@JsonIgnore
	private User sender;

	/**
	 * Message consumer
	 */
	@ManyToOne
	@JsonIgnore
	private User recipient;

	/**
	 * Content of the message, can be of different types, see
	 * com.asapp.backend.challenge.message.objects.Type class
	 */
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "content_id", insertable = true)
	@JsonProperty(value = "content")
	private Content content;


	/**
	 * @param sender    User that is producing the message
	 * @param recipient User that is consuming the message
	 * @param content   the content of the message, can be of different types, see
	 *                  com.asapp.backend.challenge.message.objects.Type class
	 */
	public Message(User sender, User recipient, Content content) {
		this.sender = sender;
		this.recipient = recipient;
		this.content = content;
		this.timestamp = Date.valueOf(LocalDate.now());
	}

	@JsonProperty("sender")
	public Integer getSenderId() {
		return this.getSender().getId();
	}

	@JsonProperty("recipient")
	public Integer getRecipientId() {
		return this.getRecipient().getId();
	}


	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}


	public void setContent(Content content) {
		content.setType(content.getType().toLowerCase());
		this.content = content;
	}
}
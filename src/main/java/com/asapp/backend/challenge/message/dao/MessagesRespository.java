package com.asapp.backend.challenge.message.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.asapp.backend.challenge.message.model.Message;
import com.asapp.backend.challenge.user.model.User;

@Repository
public interface MessagesRespository extends JpaRepository<Message, Integer> {
	@Query("SELECT m FROM Message m WHERE m.recipient = ?1")
	public Page<Message> findByRecipient(User recipient, PageRequest pageRequest);

}

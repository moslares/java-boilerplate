package com.asapp.backend.challenge.message.services;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.asapp.backend.challenge.message.dao.MessagesRespository;
import com.asapp.backend.challenge.message.model.Message;
import com.asapp.backend.challenge.user.model.User;

@Service
public class MessageServiceImpl implements MessageService {

	@Autowired
	private MessagesRespository messagesRepository;

	/**
	 * Adds a message
	 */
	public Integer addMessage(Message message) {
		message.setTimestamp(Date.valueOf(LocalDate.now()));
		Message savedMessage = messagesRepository.save(message);
		return savedMessage.getId();
	}

	/**
	 * Fetches all existing messages to a given recipient, within a range of message
	 * IDs.
	 */
	@Override
	public List<Message> getFilteredMessages(Integer recipient, Integer startingId, Integer limit) {
		if (limit == null)
			limit = 100;
		Sort sort = Sort.by("id");
		PageRequest pageRequest = PageRequest.of(0, limit.intValue(), sort);
		 List<Message> messages = null;
		try {
			User user = new User();
			user.setId(recipient);
			messages = messagesRepository.findByRecipient(user, pageRequest).getContent();
		} catch (Exception e){
			return new ArrayList<>();
		}
		return messages;
	}

}

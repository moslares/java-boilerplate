package com.asapp.backend.challenge.message.objects;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = LowerCaseCustomSerializer.class)
public enum Source {
	YOUTUBE,
	VIMEO
	
}
package com.asapp.backend.challenge.message.objects;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class LowerCaseCustomSerializer extends StdSerializer<Enum<?>> {
	private static final long serialVersionUID = -7335686872154505105L;

	public LowerCaseCustomSerializer() {
        super(Enum.class, false);
    }

	 @Override
     public void serialize(Enum<?> value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
         jgen.writeString(value.name().toLowerCase());
     }
	 
}
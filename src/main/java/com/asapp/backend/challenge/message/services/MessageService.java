package com.asapp.backend.challenge.message.services;

import java.util.List;

import com.asapp.backend.challenge.message.model.Message;

public interface MessageService {
	public Integer addMessage(Message message);

	public List<Message> getFilteredMessages(Integer recipient, Integer startingId, Integer limit);

	
	
}

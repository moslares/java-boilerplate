package com.asapp.backend.challenge.auth.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author mmoslares
 * @see 
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "challengeapp_authentication")
public class Authentication {

	/**
	 * Auth id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	protected Integer id;

	/**
	 * Token
	 */
	@Column(name = "token", updatable = true, nullable = false)
	private String token;
	

	
	public Authentication(String token) {
		this.token = token;
	}

}
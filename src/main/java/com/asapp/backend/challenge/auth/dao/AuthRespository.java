package com.asapp.backend.challenge.auth.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asapp.backend.challenge.auth.model.Authentication;

@Repository
public interface AuthRespository extends JpaRepository<Authentication, Integer>{
	
}

package com.asapp.backend.challenge.auth.objects;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.asapp.backend.challenge.user.model.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class AuthenticationTool {

	@Value(value = "${security.authentication.token.secret}")
	private String tokenSecret;

	@Value(value = "${security.authentication.token.prefix}")
	private String tokenPrefix;

	@Value(value = "${security.authentication.token.expiration-time}")
	private Integer tokenExpirationTime;

	public AuthenticationTool() {
		// default constructor
	}

	public Claims decodeJWT(String jwtString) {
		Claims claims = null;
		claims = Jwts.parser().setSigningKey(tokenSecret).parseClaimsJws(jwtString).getBody();
		
		return claims;

	}

	public String getJWTToken(User user) {
		Date expirationTime = new Date(new Date().getTime() + tokenExpirationTime);
		Map<String, Object> customClaims = new HashMap<>();
		customClaims.put("userId", user.getId());
		String jwt = "";
		try {
			jwt = Jwts.builder().setSubject(user.getUsername()).addClaims(customClaims).setExpiration(expirationTime)
					.signWith(SignatureAlgorithm.HS512, tokenSecret).compact();
		} catch (Exception e) {
			return jwt;
		}
		return tokenPrefix + " " + jwt;
	}
}
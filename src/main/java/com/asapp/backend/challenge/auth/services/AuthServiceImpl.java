package com.asapp.backend.challenge.auth.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asapp.backend.challenge.auth.dao.AuthRespository;
import com.asapp.backend.challenge.auth.model.Authentication;

@Service
public class AuthServiceImpl implements AuthService {
	@Autowired
	private AuthRespository authRepository;


	

	@Override
	public Authentication findById(Integer id) {
		Optional<Authentication> authentication = authRepository.findById(id);
		if(authentication.isPresent()) {
			return authentication.get();
		} else return null;
	}

}

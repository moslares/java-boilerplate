package com.asapp.backend.challenge.auth.services;

import com.asapp.backend.challenge.auth.model.Authentication;

public interface AuthService {

	public Authentication findById(Integer id);

		
}

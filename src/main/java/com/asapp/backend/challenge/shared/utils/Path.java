package com.asapp.backend.challenge.shared.utils;

public interface Path {
    String USERS = "/users";
    String MESSAGES = "/messages";
    String LOGIN = "/login";
    String HEALTH = "/check";
}

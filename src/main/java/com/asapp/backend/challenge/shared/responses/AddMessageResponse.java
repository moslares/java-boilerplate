package com.asapp.backend.challenge.shared.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddMessageResponse {
	private Integer id;

	public AddMessageResponse(Integer id) {
		this.id = id;
	}
}

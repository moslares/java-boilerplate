package com.asapp.backend.challenge.shared.responses;

public class ResponseUtils {
	public static final String FORBIDDEN = "Forbidden";
	public static final String GENERIC_ERROR = "Generic error";
	public static final String INVALID_USER = "Please verify the entered user (s) information";
	public static final String USER_ALLREADY_EXIST = "Username not available";
	public static final String CANT_SEND_MESSAGE_FROM_ANOTHER_USER = "You cant send a message from other source than you, check sender is the authenticated user id";
	public static final String USER_NOT_EXISTANT = "Sender or receiver does not exist";

	private ResponseUtils() {
		// default constructor
	}
}

package com.asapp.backend.challenge.shared.utils;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import com.asapp.backend.challenge.message.model.Message;
import com.asapp.backend.challenge.message.objects.LowerCaseCustomSerializer;
import com.asapp.backend.challenge.message.objects.MessageCustomDeserializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class JSONUtil {

	public static final String APPLICATION_JSON = "application/json";

	private JSONUtil() {
		
	}
	public static String dataToJson(Object data) throws IOException {		
			ObjectMapper mapper = new ObjectMapper();
			
			mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			mapper.configOverride(Map.class)
					.setInclude(JsonInclude.Value.construct(JsonInclude.Include.NON_EMPTY, null));
			
			SimpleModule module = new SimpleModule();
			module.addSerializer(new LowerCaseCustomSerializer());
			mapper.registerModule(module);

			StringWriter sw = new StringWriter();
			mapper.writeValue(sw, data);
			return sw.toString();
	}

	public static <T> Object jsonToData(String json, Class<T> objectClass) {
		ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		SimpleModule module = new SimpleModule();
		module.addDeserializer(Message.class, new MessageCustomDeserializer());
		mapper.registerModule(module);
		try {
			return mapper.readValue(json, objectClass);
		} catch (JsonProcessingException e) {
			return null;
		}
	}

}

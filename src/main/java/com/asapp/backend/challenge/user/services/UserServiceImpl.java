package com.asapp.backend.challenge.user.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.asapp.backend.challenge.auth.model.Authentication;
import com.asapp.backend.challenge.auth.objects.AuthenticationTool;
import com.asapp.backend.challenge.user.dao.UsersRespository;
import com.asapp.backend.challenge.user.model.User;

import se.simbio.encryption.Encryption;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UsersRespository usersRepository;

	@Autowired
	private AuthenticationTool authTool;

	@Value(value = "${security.password-secret-key}")
	private String key;

	@Value(value = "${security.password-secret-salt}")
	private String salt;

	public Integer addUser(User user) {
		Encryption encryption = Encryption.getDefault(key, salt, new byte[16]);

		user.setPassword(encryption.encryptOrNull(user.getPassword()));

		return usersRepository.save(user).getId();
	}

	@Override
	public User findFirstByUsername(String username) {
		return usersRepository.findFirstByUsername(username);
	}

	/**
	 * Validate user and returns a JWT Token
	 */
	@Override
	public Authentication validateUserCredentials(User userToValidate) {
		Encryption encryption = Encryption.getDefault(key, salt, new byte[16]);

		User userFromDatabase = usersRepository.findFirstByUsername(userToValidate.getUsername());

		if (userFromDatabase == null)
			return null;
		if (encryption.decryptOrNull(userFromDatabase.getPassword()).equals(userToValidate.getPassword())) {
			Authentication authentication = new Authentication();
			Authentication authenticationFromDatabase = userFromDatabase.getAuthentication();
			if (authenticationFromDatabase != null) {
				Integer authId = userFromDatabase.getAuthentication().getId();
				authentication.setId(authId);
			}
			authentication.setToken(authTool.getJWTToken(userFromDatabase).split(" ")[1]);
			userFromDatabase.setAuthentication(authentication);

			usersRepository.save(userFromDatabase);

			return userFromDatabase.getAuthentication();

		} else
			return null;

	}

	@Override
	public User findById(Integer id) {
		return usersRepository.getById(id);

	}

	@Override
	public boolean existsById(Integer id) {
		return usersRepository.existsById(id);
	}

}

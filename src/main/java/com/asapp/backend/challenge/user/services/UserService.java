package com.asapp.backend.challenge.user.services;

import com.asapp.backend.challenge.auth.model.Authentication;
import com.asapp.backend.challenge.user.model.User;

public interface UserService {
	public Integer addUser(final User user);
	
	public User findFirstByUsername(final String username);

	public User findById(final Integer username);

	public Authentication validateUserCredentials(User user);

	boolean existsById(Integer id);
}

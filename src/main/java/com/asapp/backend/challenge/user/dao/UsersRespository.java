package com.asapp.backend.challenge.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asapp.backend.challenge.user.model.User;

@Repository
public interface UsersRespository extends JpaRepository<User, Integer>{
	public User findFirstByUsername(String username);
}

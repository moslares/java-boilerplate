package com.asapp.backend.challenge.user.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.asapp.backend.challenge.auth.model.Authentication;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "challengeapp_users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	protected Integer id;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Column(name = "username", nullable = false)
	@JsonProperty("username")
	private String username;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Column(name = "password", nullable = false)
	@JsonProperty("password")
	private String password;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "authentication_id", insertable = true)
	@JsonIgnore
	private Authentication authentication;

	public User(Integer id) {
		this.id = id;
	}
	

}

package com.asapp.backend.challenge.testsmethods;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.asapp.backend.challenge.Application;
import com.asapp.backend.challenge.shared.utils.Path;
import com.asapp.backend.challenge.tests.utils.TestUtils;

/**
 * Users tests.
 * 
 * @author marco
 * 
 */

public class HealthMethods {

	private static final String HEALTH_ENDPOINT = "http://localhost:" + Application.PORT + Path.HEALTH;

	/**
	 * Test the user endpoints
	 * 
	 * @throws UnsupportedOperationException
	 * @throws IOException
	 * @throws URISyntaxException
	 */

	public void test() throws UnsupportedOperationException, IOException, URISyntaxException {
		testGetHealth();
	}

	private void testGetHealth() throws UnsupportedOperationException, IOException, URISyntaxException {
		URIBuilder builder = new URIBuilder(HEALTH_ENDPOINT);

		HttpPost request = new HttpPost(builder.build());
		TestUtils.setCommonHeaders(request, null);

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		HttpEntity entity = httpResponse.getEntity();
		String result = EntityUtils.toString(entity);

		if (null != httpResponse) {
			assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
			assertTrue(result.contains("ok"));
		}

	}

}
package com.asapp.backend.challenge.testsmethods;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.BeforeAll;

import com.asapp.backend.challenge.Application;
import com.asapp.backend.challenge.message.model.ContentImage;
import com.asapp.backend.challenge.message.model.ContentVideo;
import com.asapp.backend.challenge.message.objects.Source;
import com.asapp.backend.challenge.shared.utils.Path;
import com.asapp.backend.challenge.tests.utils.TestUtils;

/**
 * Users tests.
 * 
 * @author marco
 * 
 */
public class MessagesMethods {

	private static final String MESSAGES_ENDPOINT = "http://localhost:" + Application.PORT + Path.MESSAGES;
	private String token = null;

	@BeforeAll
	public static void setUp() {

		ContentImage contentImage = new ContentImage();
		contentImage.setType("image");
		contentImage.setUrl("http://test");
		contentImage.setWidth(333);
		contentImage.setHeight(222);

		ContentVideo contentVideo = new ContentVideo();
		contentVideo.setType("video");
		contentVideo.setUrl("http://testVideo");
		contentVideo.setSource(Source.VIMEO);

	}

	public MessagesMethods(String token) {
		this.token = token;
	}

	/**
	 * Test the messages endpoints
	 * 
	 * @throws UnsupportedOperationException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public void test() throws UnsupportedOperationException, IOException, URISyntaxException {

		String testMessageText = "{\n" + "\"sender\": 1,\n" + "\"recipient\": 1,\n" + "\"content\": {\n"
				+ "\"type\": \"text\",\n" + "\"text\": \"testtext\"\n" + "}\n" + "}";

		String testMessageImage = "{\n" + "\"sender\": 1,\n" + "\"recipient\": 1,\n" + "\"content\": {\n"
				+ "\"type\": \"image\",\n" + "\"url\": \"testurl\",\n" + "\"height\": \"23\",\n" + "\"width\": \"45\"\n"
				+ "}\n" + "}";

		String testMessageVideo = "{\n" + "\"sender\": 1,\n" + "\"recipient\": 1,\n" + "\"content\": {\n"
				+ "\"type\": \"video\",\n" + "\"url\": \"testurl\",\n" + "\"source\": \"vimeo\"\n" + "}\n" + "}";

		String testMessageWithDifferentSender = "{\n" + "\"sender\": 4444,\n" + "\"recipient\": 1,\n"
				+ "\"content\": {\n" + "\"type\": \"video\",\n" + "\"url\": \"testurl\",\n" + "\"source\": \"vimeo\"\n"
				+ "}\n" + "}";
		
		String testMessageWithUnexistantRecipiend = "{\n" + "\"sender\": 1,\n" + "\"recipient\": 333,\n"
				+ "\"content\": {\n" + "\"type\": \"video\",\n" + "\"url\": \"testurl\",\n" + "\"source\": \"vimeo\"\n"
				+ "}\n" + "}";

		testPostMessage(testMessageText);
		testPostMessage(testMessageImage);
		testPostMessage(testMessageVideo);
		testGetMessages();
		String correctToken = this.token;
		testPostMessageWithError(testMessageWithDifferentSender);
		testPostMessageWithError(testMessageWithUnexistantRecipiend);
		
		this.token = "asdasdaskdjlskdj";
		testPostMessageWithError(testMessageText);
		this.token = correctToken;
	}

	/**
	 * Test adding a message
	 * 
	 * @param bearerToken
	 * 
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	private void testPostMessage(String message) throws ClientProtocolException, IOException {
		HttpPost request = new HttpPost(MESSAGES_ENDPOINT);
		TestUtils.setCommonHeaders(request, token);

		HttpEntity entity = new ByteArrayEntity(message.getBytes("UTF-8"));

		request.setEntity(entity);
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		if (null != httpResponse) {
			assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		}
	}
	
	/**
	 * Test adding a message with error
	 * 
	 * @param bearerToken
	 * 
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	private void testPostMessageWithError(String message) throws ClientProtocolException, IOException {
		HttpPost request = new HttpPost(MESSAGES_ENDPOINT);
		TestUtils.setCommonHeaders(request, token);

		HttpEntity entity = new ByteArrayEntity(message.getBytes("UTF-8"));

		request.setEntity(entity);
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		if (null != httpResponse) {
			assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_UNAUTHORIZED));
		}
	}

	/**
	 * Test getting messages
	 * 
	 * @throws UnsupportedOperationException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private void testGetMessages() throws UnsupportedOperationException, IOException, URISyntaxException {
		URIBuilder builder = new URIBuilder(MESSAGES_ENDPOINT);
		builder.setParameter("recipient", "1").setParameter("start", "1").setParameter("limit", "100");

		HttpGet request = new HttpGet(builder.build());
		TestUtils.setCommonHeaders(request, token, false);

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		HttpEntity entity = httpResponse.getEntity();
		String result = EntityUtils.toString(entity);

		if (null != httpResponse) {
			assertNotNull(result);
		}

	}

}
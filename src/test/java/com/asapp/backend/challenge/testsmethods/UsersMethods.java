package com.asapp.backend.challenge.testsmethods;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.BeforeAll;

import com.asapp.backend.challenge.Application;
import com.asapp.backend.challenge.resources.LoginResource;
import com.asapp.backend.challenge.shared.responses.ResponseUtils;
import com.asapp.backend.challenge.shared.utils.JSONUtil;
import com.asapp.backend.challenge.shared.utils.Path;
import com.asapp.backend.challenge.tests.utils.TestUtils;
import com.asapp.backend.challenge.user.model.User;

/**
 * Users tests.
 * 
 * @author marco
 * 
 */

public class UsersMethods {

	private static User testUser = new User();

	private static final String USERS_ENDPOINT = "http://localhost:" + Application.PORT + Path.USERS;
	private static final String LOGIN_ENDPOINT = "http://localhost:" + Application.PORT + Path.LOGIN;
	private static final String USERNAME = "test_user";
	private static final String PASSWORD = "test_password";

	@BeforeAll
	public static void setUp() {
		testUser.setUsername(USERNAME);
		testUser.setPassword(PASSWORD);
	}

	/**
	 * Test the user endpoints
	 * 
	 * @throws UnsupportedOperationException
	 * @throws IOException
	 */
	public String test() throws UnsupportedOperationException, IOException {
		createUser();
		String token = loginUser();
		loginInvalidUser();
		createExistingUserWithError();
		createUserWithError("{}");
		return token;
	}

	public String loginUser() throws UnsupportedOperationException, IOException {
		HttpPost request = new HttpPost(LOGIN_ENDPOINT);
		TestUtils.setCommonHeaders(request, null);
		HttpEntity entity = new ByteArrayEntity(JSONUtil.dataToJson(testUser).getBytes("UTF-8"));

		request.setEntity(entity);
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		InputStream content = httpResponse.getEntity().getContent();
		BufferedReader contentReader = new BufferedReader(new InputStreamReader(content, UTF_8));

		String httpResponseContent = contentReader.lines().collect(Collectors.joining());
		LoginResource authenticationResponse = (LoginResource) JSONUtil.jsonToData(httpResponseContent,
				LoginResource.class);
		if (null != httpResponse) {
			assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
			assertThat(authenticationResponse.getToken().length(), greaterThan(0));
		}
		return authenticationResponse.getToken();

	}

	public void loginInvalidUser() throws UnsupportedOperationException, IOException {
		HttpPost request = new HttpPost(LOGIN_ENDPOINT);
		TestUtils.setCommonHeaders(request, null);
		User invalidUser = new User();
		invalidUser.setUsername("not_existant");
		invalidUser.setPassword("not_existant");
		HttpEntity entity = new ByteArrayEntity(JSONUtil.dataToJson(invalidUser).getBytes("UTF-8"));

		request.setEntity(entity);
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		if (null != httpResponse) {
			assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_UNAUTHORIZED));
		}

	}

	/**
	 * Test a user creation not providing the necessary parameters
	 * 
	 * @param bearerToken
	 * 
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	private void createExistingUserWithError() throws ClientProtocolException, IOException {
		HttpPost request = new HttpPost(USERS_ENDPOINT);
		TestUtils.setCommonHeaders(request, null);
		HttpEntity entity = new ByteArrayEntity(JSONUtil.dataToJson(testUser).getBytes("UTF-8"));

		request.setEntity(entity);
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		InputStream content = httpResponse.getEntity().getContent();
		BufferedReader contentReader = new BufferedReader(new InputStreamReader(content, UTF_8));

		String httpResponseContent = contentReader.lines().collect(Collectors.joining());

		assertEquals(ResponseUtils.USER_ALLREADY_EXIST, httpResponseContent);
	}

	/**
	 * Test a user creation providing the necessary parameters
	 * 
	 * @param bearerToken
	 * 
	 * @throws UnsupportedOperationException
	 * @throws IOException
	 */
	public void createUser() throws UnsupportedOperationException, IOException {
		HttpPost request = new HttpPost(USERS_ENDPOINT);
		TestUtils.setCommonHeaders(request, null);
		HttpEntity entity = new ByteArrayEntity(JSONUtil.dataToJson(testUser).getBytes("UTF-8"));

		request.setEntity(entity);
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		InputStream content = httpResponse.getEntity().getContent();
		BufferedReader contentReader = new BufferedReader(new InputStreamReader(content, UTF_8));

		String httpResponseContent = contentReader.lines().collect(Collectors.joining());
		User resultantUser = (User) JSONUtil.jsonToData(httpResponseContent, User.class);
		if (null != httpResponse) {
			assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
			assertThat(resultantUser.getId(), greaterThan(0));
		}

	}

	/**
	 * Test a user creation not providing the necessary parameters
	 * 
	 * @param bearerToken
	 * 
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	private void createUserWithError(String message) throws ClientProtocolException, IOException {
		HttpPost request = new HttpPost(USERS_ENDPOINT);
		TestUtils.setCommonHeaders(request, null);
		HttpEntity entity = new ByteArrayEntity(JSONUtil.dataToJson("message").getBytes("UTF-8"));
		request.setEntity(entity);
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		;

		InputStream content = httpResponse.getEntity().getContent();
		BufferedReader contentReader = new BufferedReader(new InputStreamReader(content, UTF_8));
		String httpResponseContent = contentReader.lines().collect(Collectors.joining());

		assertEquals(ResponseUtils.INVALID_USER, httpResponseContent);

	}

}
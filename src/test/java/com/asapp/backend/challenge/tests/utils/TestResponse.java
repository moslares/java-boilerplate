package com.asapp.backend.challenge.tests.utils;

/**
 * Test response Object
 * @author mmoslares
 *
 */
public class TestResponse {

	public final String body;
	public final int status;

	/**
	 * @param status response status
	 * @param body response body
	 */
	public TestResponse(int status, String body) {
		this.status = status;
		this.body = body;
	}
}
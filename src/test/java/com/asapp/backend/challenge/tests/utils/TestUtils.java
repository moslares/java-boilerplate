package com.asapp.backend.challenge.tests.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.methods.HttpUriRequest;

public class TestUtils {

	public static void setCommonHeaders(HttpUriRequest request, String token) {
		setCommonHeaders(request, token, true);
	}
	public static void setCommonHeaders(HttpUriRequest request, String token, boolean isJsonRequest) {

		Map<String, String> defaultHeaders = new HashMap<>();
		if (isJsonRequest) {
			defaultHeaders.put("Content-Type", "application/json");
		}
		defaultHeaders.put("Accept", "*/*");
		defaultHeaders.put("Accept-Encoding", "gzip, deflate, br");
		defaultHeaders.put("Connection", "keep-alive");
		if (token != null) {
			defaultHeaders.put("Authorization", "Bearer " + token);
		}
		for (String header : defaultHeaders.keySet()) {
			request.setHeader(header, defaultHeaders.get(header));
		}
	}

}

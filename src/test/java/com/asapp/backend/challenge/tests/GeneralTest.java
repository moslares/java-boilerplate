package com.asapp.backend.challenge.tests;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.asapp.backend.challenge.tests.utils.TraceUnitExtension;
import com.asapp.backend.challenge.testsmethods.HealthMethods;
import com.asapp.backend.challenge.testsmethods.MessagesMethods;
import com.asapp.backend.challenge.testsmethods.UsersMethods;
@ExtendWith(TraceUnitExtension.class)
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource("classpath:application-test.yml")
class GeneralTest {

	private String token;
	@Test
	void test() throws UnsupportedOperationException, IOException, URISyntaxException {
		testHealt();
		this.token = testUsers();
		testMessages();
	}
	
	void testHealt() throws UnsupportedOperationException, IOException, URISyntaxException {
		HealthMethods healthTest = new HealthMethods();
		healthTest.test();
	}
	
	private String testUsers() throws UnsupportedOperationException, IOException {
		UsersMethods userTest = new UsersMethods();
		UsersMethods.setUp();
		return userTest.test();
	}
	
	void testMessages() throws UnsupportedOperationException, IOException, URISyntaxException {
		MessagesMethods messagesTest = new MessagesMethods(token);
		MessagesMethods.setUp();
		messagesTest.test();
	}
	

}

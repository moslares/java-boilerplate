package com.asapp.backend.challenge.tests.utils;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import com.asapp.backend.challenge.Application;

import spark.Spark;

public class TraceUnitExtension implements AfterAllCallback, BeforeAllCallback {

	

	@Override
	public void beforeAll(ExtensionContext context) throws Exception {
		Application.main(null);		
	}

	@Override
	public void afterAll(ExtensionContext context) throws Exception {
		Spark.stop();		
	}
}

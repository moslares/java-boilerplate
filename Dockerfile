FROM gradle:7.1.1-jdk8 AS build
RUN mkdir -p /opt/project
COPY . /opt/project
WORKDIR /opt/project
RUN gradle --version
RUN gradle build
 
 
FROM openjdk:8-jre-slim
RUN groupadd -r asappgroup && \
    useradd -g asappgroup -d /home/user/asappuser -s /sbin/nologin -c "Asapp chalenge user" asappuser && \
    mkdir -p /opt/java/applications && \
    chown -R asappuser:asappgroup /opt/java/applications
    
COPY --from=build /opt/project/build/libs/java-boilerplate-1.0.0.jar /opt/java/applications/java-boilerplate-1.0.0.jar
WORKDIR /opt/java/applications
USER asappuser
CMD "java" "-jar" "java-boilerplate-1.0.0.jar"
# ASAPP Chat Backend Challenge v1
### Overview
This is a java based boilerplate which runs an HTTP Server configured to answer the endpoints defined in 
[the challenge you received](https://backend-challenge.asapp.engineering/).
All endpoints are configured in src/main/java/com/asapp/backend/challenge/Application.java and if you go deeper to the
Routes and Filters passed as second parameters, you will find all the implementations needed for the challenge.



------------
:exclamation:  Application port has been changed to 8888

------------
### Code coverage and quality
In the project, you can see a bitbucket-pipelines.yml that includes a pipeline that connect this repository with sonarcloud.
You can see the following code quality indicators at the main bitbucket project url:

:white_check_mark: Bugs

:white_check_mark: Vulnerabilities

:white_check_mark: Security Hotspots

:white_check_mark: Code Smells

:white_check_mark: Coverage

:white_check_mark: Duplications

------------

### How to build and run as standalone
```
gradle build
gradle run
```

------------


### How to build and run it in docker
```
docker build -t java-boilerplate:1.0.0 .
docker run java-boilerplate:1.0.0
docker push java-boilerplate:1.0.0
```

------------


### How to deploy and run in minikube
Ensure docker and minikube are up and running
```
minikube status
docker info 
```

Go to the k8s folder, and execute
```
kubectl config use-context minikube
```

For linux run
```
eval $(minikube -p minikube docker-env)
```

Or else for windows run
```
@FOR /f "tokens=* delims=^L" %i IN ('minikube docker-env') DO %i
```

Then build the image
```
docker build . --tag local/java-boilerplate
kubectl create -f deployment.yaml
```

Review pod status with the following command (just one pod was configured for the POC)
```
kubectl get pods
```

Wait until pod is started, and then (change the pod name with the one resultant from the last command)
```
kubectl port-forward pod/java-boilerplate-xxxxx 8888:8888
```


Tha application is now ready to be accessed throug port 8888 of the local machine where this commands are being executed.


------------

## Testing
The repository is shipped with example http operations based in postman tool, you can import the postman collections into your postman workspace.  The available collections are in the following paths:
```
postman/Health.postman_collection.json
postman/Users.postman_collection.json
postman/Messages.postman_collection.json
```





